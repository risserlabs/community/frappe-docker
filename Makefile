include mkpm.mk
ifneq (,$(MKPM_READY))
include $(MKPM)/gnu
include $(MKPM)/mkchain
include $(MKPM)/envcache
include $(MKPM)/dotenv

DOCKER ?= docker

.PHONY: clean
clean: | sudo ##
	$(DOCKER) rm -f $$($(DOCKER) ps -aq)
	$(ECHO) y | $(DOCKER) volume prune
	$(ECHO) y | $(DOCKER) network prune
	@$(SUDO) $(RM) -rf development/frappe-bench
	-@$(MKCACHE_CLEAN)
	-@$(GIT) clean -fXd \
		$(MKPM_GIT_CLEAN_FLAGS) \
		$(NOFAIL)

-include $(call actions)

CACHE_ENVS += \
	DOCKER

endif
